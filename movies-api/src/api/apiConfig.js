const apiConfig = {
  baseUrl: "https://api.themoviedb.org/3/",
  apiKey: "88356f8fb907d08725a36fdf1f29d057",
  originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
  w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`,
};

export default apiConfig;
